/**
 * 
 * @link https://leetcode.com/problems/two-sum/
 * @param {number[]} nums
 * @param {number[]}target 
 */
export var twoSum = (nums: number[], target: number): number[] => {
    if (nums) {
        let cache = {};
        for (let index = 0; index < nums.length; index++) {
            const num = nums[index];
            if (undefined != cache[target - num]) {
                return [cache[target - num], index];
            } else {
                cache[num] = index;
            }
        }
        return null;
    }
};