/**
 * @link https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
 * @param {string} source 
 */
export var lengthOfLongestSubstring: (source: string) => number = (source: string): number => {
    //
    if (!source) {
        return 0;
    }
    let result: string = source[0];
    let cache = {};
    // i = start, j = end;
    for (let i: number = 0; i < source.length; i++) {
        for (let j = i; j < source.length; j++) {
            let char = source[j];
            let str: string = source.substring(i, j + 1);
            if (undefined === cache[char]) {
                cache[char] = j;
                if (result.length < str.length) {
                    result = str;
                }
            } else {
                cache = {};
                break;
            }
        }
    }
    return result.length;
};