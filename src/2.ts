/**
 * @link https://leetcode.com/problems/add-two-numbers/description/
 */
export class ListNode {
    public val: number;
    public next: ListNode;
    constructor(val: number) {
        this.val = val;
        this.next = null;
    }
}
/**
 * @param {ListNode} l11
 * @param {ListNode} l22
 * @return {ListNode}
 */
export var addTwoNumbers = (l1: ListNode, l2: ListNode): ListNode => {
    let result: ListNode = null,
        step: ListNode = result,
        carry: number = 0;
    while (l1) {
        if (null === step) {
            result = step = new ListNode(carry);
        }
        let sum = carry;
        if (null != l1.val) {
            sum += l1.val;
            l1 = l1.next;
        }
        if (l2 && null != l2.val) {
            sum += l2.val;
            l2 = l2.next;
        }
        sum > 9 ? carry = 1 : carry = 0;
        step.val = sum % 10;
        if (!l1) {
            l1 = l2;
            l2 = null;
        }
        if (l1 || carry > 0) {
            step = step.next = new ListNode(1);
        }
    }
    return result;
};