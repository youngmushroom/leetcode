import { } from "jasmine";
import { addTwoNumbers, ListNode } from "../2";
describe("lengthOfLongestSubstring invalid test", () => {
    it("342 + 465 = 807", () => {
        // Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
        // Output: 7 -> 0 -> 8
        // Explanation: 342 + 465 = 807.
        let l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(3);
        let l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);
        let l3 = new ListNode(7);
        l3.next = new ListNode(0);
        l3.next.next = new ListNode(8);
        expect(addTwoNumbers(l1, l2)).toEqual(l3);
    });
    it("100 + 10 = 110", () => {
        // Input: (0 -> 0 -> 1) + (0-> 1)
        // Output: 0->1->1
        // Explanation: 100 + 10 = 110.
        let l1 = new ListNode(0);
        l1.next = new ListNode(0);
        l1.next.next = new ListNode(1);

        let l2 = new ListNode(0);
        l2.next = new ListNode(1);

        let l3 = new ListNode(0);
        l3.next = new ListNode(1);
        l3.next.next = new ListNode(1);
        expect(addTwoNumbers(l1, l2)).toEqual(l3);
    });
    it("5 + 5 = 10", () => {
        // Input: (0 -> 0 -> 1) + (0-> 1)
        // Output: 0->1->1
        // Explanation: 100 + 10 = 110.
        let l1 = new ListNode(5);
        let l2 = new ListNode(5);
        let l3 = new ListNode(0);
        l3.next = new ListNode(1);
        expect(addTwoNumbers(l1, l2)).toEqual(l3);
    });
    it("0 + 37 = 37", () => {
        let l1 = new ListNode(0);
        let l2 = new ListNode(7);
        l2.next = new ListNode(3);
        let l3 = new ListNode(7);
        l3.next = new ListNode(3);
        expect(addTwoNumbers(l1, l2)).toEqual(l3);
    });
});