import { } from "jasmine";
import { lengthOfLongestSubstring } from "../3";
describe("lengthOfLongestSubstring", () => {
    it("EmptyOrNullOrUndefined", () => {
        expect(lengthOfLongestSubstring("")).toBe(0);
        expect(lengthOfLongestSubstring(null)).toBe(0);
        expect(lengthOfLongestSubstring(undefined)).toBe(0);
    });
    it("NoLoop", () => {
        expect(lengthOfLongestSubstring("c")).toBe(1);
        expect(lengthOfLongestSubstring("au")).toBe(2);
    });
    it("WithLoop", () => {
        expect(lengthOfLongestSubstring("pwwkew")).toBe(3);
        expect(lengthOfLongestSubstring("abcabcbb")).toBe(3);
        expect(lengthOfLongestSubstring("bbb")).toBe(1);
    });
});